build=$1

if [ "$build" == "" ]; then
    build="debug"
fi

#linkopts="+L +S"

case "$build" in
    "release" ) buildopts="-D +O";;
    "debug" ) buildopts="+D -O -R +S";;
esac

echo "Build [$build] with options [ $buildopts ]"

if [ ! -d temp ]; then
	mkdir temp
fi
rm -rf ./temp/*

if [ ! -d lib ]; then
	mkdir lib
fi
rm -rf ./lib/*

if [ ! -d bin ]; then
	mkdir bin
fi
rm -rf ./bin/*

rootdir=`pwd`
srcdir=$rootdir/src
libdir=$rootdir/lib
bindir=$rootdir/bin
orcadir=$rootdir/ORCA

cd $srcdir
folders=`ls -d */`

for folder in $folders; do
    cd $srcdir/$folder

    if [ -f *.c ]; then
    	files=`ls -d *.c`
    	for file in $files; do
        	fileid=`echo "$file" | cut -d'.' -f1`
        	mkdir $libdir/$folder
        	echo "Compiling $file"
        	iix compile $buildopts -R -P $file keep=$libdir/$folder/$fileid
    	done
    fi

    if [ -f *.asm ]; then
    	files=`ls -d *.asm`
    	for file in $files; do
        	fileid=`echo "$file" | cut -d'.' -f1`
        	mkdir $libdir/$folder
        	echo "Compiling $file"
        	iix compile $buildopts $file keep=$libdir/$folder/$fileid
    	done
    fi
    cd $srcdir
done

echo Linking NTEST
mv $libdir/ntest/main.root $libdir/ntest/main.ROOT
#iix link $libdir/ntest/main $libdir/utils/utils $libdir/w5500/w5500 $libdir/spi/spi $libdir/socket/socket $libdir/spiasm/spiasm keep=$bindir/ntest

#-C -X +L 

echo Linking SPIDL
iix link $linkopts \
	$libdir/spidl/spidl_main \
	$libdir/utils/utils \
	$libdir/w5500/w5500 \
	$libdir/spi/spi \
	$libdir/spiasmread/spiasmread \
	$libdir/spiasmwrite/spiasmwrite \
 	$libdir/socket/socket \
	$libdir/inih/ini \
	keep=$bindir/spidl
