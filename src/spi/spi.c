#include "spi.h"

#pragma noroot

DWORD __spi_device_number = 0;

#define SPI_READ_CYCLES 1

#define A2_AN0_OFF  0xc058
#define A2_AN0_ON   0xc059
#define A2_AN1_OFF  0xc05a
#define A2_AN1_ON   0xc05b
#define A2_AN2_OFF  0xc05c
#define A2_AN2_ON   0xc05d
#define A2_AN3_OFF  0xc05e
#define A2_AN3_ON   0xc05f
#define A2_PB3      0xc060

#define SSEL_OFF (*((BYTE*)A2_AN0_OFF)  = (BYTE)0xA0)
#define SSEL_ON  (*((BYTE*)A2_AN0_ON)   = (BYTE)0xA0)
#define SCLK_OFF (*((BYTE*)A2_AN1_OFF)  = (BYTE)0xA0)
#define SCLK_ON  (*((BYTE*)A2_AN1_ON)   = (BYTE)0xA0)
#define MOSI_OFF (*((BYTE*)A2_AN3_OFF)  = (BYTE)0xA0)
#define MOSI_ON  (*((BYTE*)A2_AN3_ON)   = (BYTE)0xA0)
#define MISO_GET ((BYTE)(*((BYTE*)A2_PB3)))

void spi_device_init(spi_device_t** device_out) {
    if (!device_out) return;
    spi_device_t* device = (spi_device_t*) malloc(sizeof(spi_device_t));
    memset(device,0,sizeof(spi_device_t));
    device->id = ++__spi_device_number;
    *device_out = device;
}

void spi_begin_trans(spi_device_t *device) {
    if (!device) return;
    SCLK_OFF;
    SSEL_OFF;
}

void spi_end_trans(spi_device_t *device) {
    if (!device) return;
    SSEL_ON;
    SCLK_OFF;
}

extern WORD spisendb(BYTE *buffer, WORD numberOfBytes);
void spi_write(spi_device_t* device, BYTE *txbuf, WORD txsize) {
    if (!txbuf) { printf("no txbuf\n"); return; }
    if (!txsize) { printf("no txsize\n"); return; }
    if (!device) { printf("no device\n"); return; }
    int ts = spisendb(txbuf, txsize);
}

extern WORD spireadb(BYTE *buffer, WORD numberOfBytes);
void spi_read(spi_device_t* device, BYTE* rxbuf, WORD rxsize) {
    if (!rxbuf) return;
    if (!rxsize) return;
    if (!device) return;
    int tr=spireadb(rxbuf, rxsize);
}

 void spi_read2(spi_device_t* device, BYTE *rxbuf, WORD rxsize) {

     if (!rxbuf) return;
     if (!rxsize) return;
     if (!device) return;
    
    BYTE rxData;

    register BYTE mask = 0x80;

    BYTE goodRxCount;
    BYTE goodRxState;
    BYTE tempRxState;
    BYTE* bufpos = rxbuf;

    while(rxsize--)
    {
        rxData = 0;
        mask = 0x80;

        while(mask)
        {
            goodRxCount = 0;
            goodRxState = 0;
            
            int p = 0xF9F9;
            SCLK_ON;

readLoop:

            tempRxState = (MISO_GET & 0x80);
            if (tempRxState==goodRxState) { goodRxCount++; }
            else { goodRxState = tempRxState; goodRxCount = 0; }
            if (goodRxCount < SPI_READ_CYCLES) goto readLoop;

            if (goodRxState) rxData |= mask;
            mask = mask >> 1;

            SCLK_OFF;
        }

        *bufpos++ = rxData;

    }
 }


