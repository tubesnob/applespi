
#include "../defaults.h"
#include "../utils/utils.h"

#ifndef __SPI_DOT_H__
#define __SPI_DOT_H__

#define SPI_OK              0x0000
#define SPI_ERR_INIT        0xD501
#define SPI_ERR_BADARG      0xD502

typedef struct {
    WORD id;
 } spi_device_t;

void spi_device_init(spi_device_t** device_out);
void spi_begin_trans(spi_device_t *device);
void spi_end_trans(spi_device_t *device);
void spi_write(spi_device_t* device, BYTE* txbuf, WORD txsize);
void spi_read(spi_device_t* device, BYTE* rxbuf, WORD rxsize);

#endif
