#include "socket.h"

#pragma noroot

DWORD __socket_id = 0;

int socket_create(spi_device_t* device, BYTE protocol, WORD source_port, socket_t** socket)
{
   BYTE sn = 0;
   BYTE status = 0;

   *socket = NULL;

   for(sn=0; sn < W5500_MAX_SOCKETS; sn++) {
      w5500_get_socket_STATUS(device, sn, &status);
      if (status==W5500_SOCKET_STATUS_CLOSED) 
         break;
   };

   if (sn >= W5500_MAX_SOCKETS) {
         // try to close any sockets are in an intermittent state
         BYTE found = 0;
         BYTE status;
         for(sn=0; sn < W5500_MAX_SOCKETS && !found; sn++) {
            w5500_get_socket_STATUS(device, sn, &status);
            switch(status) {
                  case W5500_SOCKET_STATUS_LAST_ACK:
                  case W5500_SOCKET_STATUS_TIME_WAIT:
                  case W5500_SOCKET_STATUS_FIN_WAIT:
                  case W5500_SOCKET_STATUS_CLOSING:
                        w5500_set_socket_COMMAND(device, sn, W5500_SOCKET_CMD_CLOSE);
                        found = 1;
                        break;
            }
         }
   }

   if (sn >= W5500_MAX_SOCKETS)
      return SOCKET_NORESOURCES;

   socket_t* ss = (socket_t*) malloc(sizeof(socket_t));
   ss->id = __socket_id++;
   ss->number = sn;
   ss->source_port = source_port;
   ss->protocol = protocol; 
   ss->spi = device;

   w5500_set_socket_SRCPORT(ss->spi, ss->number, ss->source_port);
   w5500_set_socket_MODE(ss->spi, ss->number, W5500_SOCKET_MODE_TCP);
   w5500_set_socket_COMMAND(ss->spi, ss->number, W5500_SOCKET_CMD_OPEN);
   
   *socket = ss;

   return SOCKET_OK;
   
} 

int socket_close(socket_t* socket)
{
      return w5500_set_socket_COMMAND(socket->spi, socket->number, W5500_SOCKET_CMD_CLOSE);
}

int socket_connect(socket_t* socket) {
      w5500_set_socket_DESTIP(socket->spi, socket->number, (BYTE*) socket->dest_ip);
      w5500_set_socket_DESTPORT(socket->spi, socket->number, socket->dest_port);
      w5500_set_socket_COMMAND(socket->spi, socket->number, W5500_SOCKET_CMD_CONNECT);
}

int socket_refresh(socket_t *socket) {
      w5500_get_socket_STATUS(socket->spi, socket->number, &socket->status);
}

int socket_disconnect(socket_t* socket) {
      w5500_set_socket_COMMAND(socket->spi, socket->number, W5500_SOCKET_CMD_DISCON);
}

int socket_send(socket_t *socket, BYTE *buf, WORD length) {

      WORD txBufferFreeSize = 0;
      WORD bytesLeftToSend = length;
      BYTE socketInterruptStatus = 0;
      BYTE socketStatus = 0;
      BYTE *bufpos = buf;

      while (bytesLeftToSend) {

            // check the socket status. if it is not established, we cannot send data
            w5500_get_socket_STATUS(socket->spi, socket->number, &socketStatus);
            if (socketStatus != W5500_SOCKET_STATUS_ESTABLISHED) {
                  return SOCKET_ERR_INVALIDARGUMENT;
            }

            // wait until we get the send ok signal from the socket
            while (!socketInterruptStatus & W5500_SOCKET_INTERRUPT_SENDOK) {
                  w5500_get_socket_INTERRUPT(socket->spi, socket->number, &socketInterruptStatus);
            }

            // we are clear to send ; get the free size available in the tx buffer...
            w5500_get_socket_TX_FREESIZE(socket->spi, socket->number, &txBufferFreeSize);

            // if there is no room in the buffer... loop until we have some space free
            if (!txBufferFreeSize) {
                  continue;
            }

            WORD bytesToSendThisPass = bytesLeftToSend;
            if (bytesLeftToSend > txBufferFreeSize)
                  bytesToSendThisPass = txBufferFreeSize;

            w5500_socket_write_data(socket->spi, socket->number, bufpos, bytesToSendThisPass);

            w5500_set_socket_COMMAND(socket->spi, socket->number, W5500_SOCKET_CMD_SEND);

            bytesLeftToSend -= bytesToSendThisPass;
            bufpos += bytesToSendThisPass;

      }

      return SOCKET_OK;
}
int socket_receive_available(socket_t *socket, WORD *size) {
      w5500_get_socket_RX_RECVSIZE(socket->spi, socket->number, size);
      return SOCKET_OK;
}
int socket_receive(socket_t *socket, BYTE *buf, WORD length) {


      WORD socketBytesAvailable = 0;
      WORD bytesLeft = length;
      BYTE socketInterruptStatus = 0;
      BYTE socketStatus = 0;
      BYTE *bufpos = buf;

      while (bytesLeft) {

            // check the socket status. if it is not established, we cannot send data
            w5500_get_socket_STATUS(socket->spi, socket->number, &socketStatus);
            if (socketStatus != W5500_SOCKET_STATUS_ESTABLISHED) {
                  return SOCKET_ERR_INVALIDARGUMENT;
            }

            // wait until we get the send ok signal from the socket
            while (!socketInterruptStatus & W5500_SOCKET_INTERRUPT_RECV) {
                  w5500_get_socket_INTERRUPT(socket->spi, socket->number, &socketInterruptStatus);
            }

            // we are clear to receive ; get the free size available in the tx buffer...
            w5500_get_socket_RX_RECVSIZE(socket->spi, socket->number, &socketBytesAvailable);

            // if there is no room in the buffer... loop until we have some space free
            if (!socketBytesAvailable) {
                  continue;
            }

            WORD bytesToRead = socketBytesAvailable;
            if (bytesToRead > bytesLeft)
                  bytesToRead = bytesLeft;

            WORD rxPtr = 0;
            w5500_get_socket_RX_READPTR(socket->spi, socket->number, &rxPtr);
            w5500_socket_read_data(socket->spi, socket->number, bufpos, bytesToRead);
            w5500_set_socket_COMMAND(socket->spi, socket->number, W5500_SOCKET_CMD_RECV);

            w5500_get_socket_RX_READPTR(socket->spi, socket->number, &rxPtr);

            //DEBUG_LOG("Read %d bytes from socket\n",bytesToRead);
            //for(int pix=0; pix < bytesToRead; pix++) {
                  //DEBUG_LOG("%x:",bufpos[pix]);
            //}
            //DEBUG_LOG("\nDUMP DONE\n");

            bytesLeft -= bytesToRead;
            bufpos += bytesToRead;



      }



      return SOCKET_OK;
}


char* iptostr(ip_address_t ip, char* buf) {
      sprintf(buf, "%d.%d.%d.%d\0", ip.a0, ip.a1, ip.a2, ip.a3);
      return buf;
}

int strtoip(const char *str, ip_address_t *ip) {
      int a0, a1, a2, a3;
      sscanf(str,"%i.%i.%i.%i", &a0, &a1, &a2, &a3);
      ip->a0 = (BYTE) a0;
      ip->a1 = (BYTE) a1;
      ip->a2 = (BYTE) a2;
      ip->a3 = (BYTE) a3;
}

char* mactostr(hw_address_t mac, char *buf) {
      sprintf(buf, "%x:%x:%x:%x:%x:%x\0", mac.a0, mac.a1, mac.a2, mac.a3, mac.a4, mac.a5);
      return buf;
}

int strtomac(const char *str, hw_address_t *mac) {
      int a0, a1, a2, a3, a4, a5;
      sscanf(str,"%x:%x:%x:%x:%x:%x", &a0, &a1, &a2, &a3, &a4, &a5);
      mac->a0 = (BYTE) a0;
      mac->a1 = (BYTE) a1;
      mac->a2 = (BYTE) a2;
      mac->a3 = (BYTE) a3;
      mac->a4 = (BYTE) a4;
      mac->a5 = (BYTE) a5;
}

void dump_socketStatus(socket_t* socket)
{
        BYTE socketStatus = 0;
        w5500_get_socket_STATUS(socket->spi, socket->number, &socketStatus);
        printf("Socket [%d]\n",socket->number);
        printf("  STATUS = ");
        switch(socketStatus) {
                case W5500_SOCKET_STATUS_CLOSED:      printf("CLOSED\n"); break;
                case W5500_SOCKET_STATUS_INIT:        printf("INIT\n"); break;
                case W5500_SOCKET_STATUS_LISTEN:      printf("LISTEN\n"); break;
                case W5500_SOCKET_STATUS_SYNSENT:     printf("SYN_SENT\n"); break;
                case W5500_SOCKET_STATUS_SYNRECV:     printf("SYN_RECEIVED\n"); break;
                case W5500_SOCKET_STATUS_ESTABLISHED: printf("ESTABLISHED\n"); break;
                case W5500_SOCKET_STATUS_FIN_WAIT:    printf("FIN_WAIT\n"); break;
                case W5500_SOCKET_STATUS_CLOSING:     printf("CLOSING\n"); break;
                case W5500_SOCKET_STATUS_TIME_WAIT:   printf("TIME_WAIT\n"); break;
                case W5500_SOCKET_STATUS_CLOSE_WAIT:  printf("CLOSE_WAIT\n"); break;
                case W5500_SOCKET_STATUS_LAST_ACK:    printf("LAST_ACK\n"); break;
                case W5500_SOCKET_STATUS_UDP:         printf("UDP\n"); break;
                case W5500_SOCKET_STATUS_IPRAW:       printf("IPRAW\n"); break;
                case W5500_SOCKET_STATUS_MACRAW:      printf("MACRAW\n"); break;
                case W5500_SOCKET_STATUS_PPPOE:       printf("PPPOE\n"); break;
                default : printf("UNKNOWN [%x]\n",socketStatus); break;
        }

        BYTE protocol;
        w5500_get_socket_MODE(socket->spi, socket->number, &protocol);
        printf("  MODE = %x\n",protocol);

        WORD source_port;
        w5500_get_socket_SRCPORT(socket->spi, socket->number, &source_port);
        printf("  SOURCE PORT = %d\n",source_port);

        ip_address_t dest_ip;
        w5500_get_socket_DESTIP(socket->spi, socket->number, (BYTE*) &dest_ip);
        
        char ipBuf[32];
        iptostr(dest_ip, ipBuf);
        printf("  DEST IP = %s\n",ipBuf);
        
        WORD dest_port;
        w5500_get_socket_DESTPORT(socket->spi, socket->number, &dest_port);
        printf("  DEST PORT = %d\n",dest_port);

}



