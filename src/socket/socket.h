
#include "../defaults.h"
#include "../utils/utils.h"
#include "../w5500/w5500.h"

#ifndef __SOCKET_DOT_H__
#define __SOCKET_DOT_H__

#define SOCKET_PROTOCOL_IP    0x00
#define SOCKET_PROTOCOL_ICMP  0x01
#define SOCKET_PROTOCOL_IGMP  0x02
#define SOCKET_PROTOCOL_TCP   0x06
#define SOCKET_PROTOCOL_UDP   0x11
#define SOCKET_PROTOCOL_RAW   0xFF

#define SOCKET_OK                   0x0000
#define SOCKET_ERR_INVALIDARGUMENT  0x1000
#define SOCKET_NORESOURCES          0x3000

#define SOCKET_STATUS_CLOSED      0x00
#define SOCKET_STATUS_INIT        0x13
#define SOCKET_STATUS_LISTEN      0x14
#define SOCKET_STATUS_ESTABLISHED 0x17
#define SOCKET_STATUS_UDP         0x22

typedef struct {
   BYTE a0;
   BYTE a1;
   BYTE a2;
   BYTE a3;
} ip_address_t;

typedef struct {
   BYTE a0;
   BYTE a1;
   BYTE a2;
   BYTE a3;
   BYTE a4;
   BYTE a5;
} hw_address_t;

typedef struct {
   BYTE  number;
   BYTE  protocol;
   BYTE  status;
   WORD  id;
   WORD  source_port;
   WORD  dest_port;
   ip_address_t dest_ip;
   spi_device_t* spi;
} socket_t;

typedef struct {
   spi_device_t* spi;

} socket_hw_t;

int socket_create(spi_device_t* device, BYTE protocol, WORD source_port, socket_t** socket);
int socket_close(socket_t* socket);
int socket_connect(socket_t* socket);
int socket_disconnect(socket_t* socket);

int socket_send(socket_t *socket, BYTE *buf, WORD length);
int socket_receive_available(socket_t *socket, WORD *size);
int socket_receive(socket_t *socket, BYTE *buf, WORD length);

int socket_refresh(socket_t *socket);

char* iptostr(ip_address_t ip, char* buf);
int strtoip(const char *str, ip_address_t* ip);
char* mactostr(hw_address_t mac, char *buf);
int strtomac(const char *str, hw_address_t* mac);




void dump_socketStatus(socket_t* socket);

#endif