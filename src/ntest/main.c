#include "../defaults.h"
#include "../utils/utils.h"
#include "../spi/spi.h"
#include "../w5500/w5500.h"
#include "../socket/socket.h"

void dump_socketStatus(socket_t* socket);

int main(int argc, char** argv)
{

        int counter;
        int counter2;
        spi_device_t* device;

        BYTE hwaddr[]   = { 0x80, 0x70, 0x60, 0x50, 0x40, 0x30 };
        BYTE ipaddr[]   = { 192, 168, 100, 133 };
        BYTE mask[]     = { 255, 255, 255, 0 };
        BYTE gwaddr[]   = { 192, 168, 100, 254};

        counter = 0;
        
        DEBUG_LOG("Initializing SPI Device\n");
        spi_device_init(&device);

        DEBUG_LOG("Device ID [%d]\n",device->id);

        DEBUG_LOG("Initializing W5500\n");
        w5500_init(device);

        DEBUG_LOG("Resetting W5500\n");
        w5500_reset(device);

        waitMilliseconds(1000);

        DEBUG_LOG("Setting MAC\n");
        w5500_set_SRCMAC(device, hwaddr);

        DEBUG_LOG("Setting Source IP\n");
        w5500_set_SRCIP(device, ipaddr);

        DEBUG_LOG("Setting Subnet Mask\n");
        w5500_set_SUBMASK(device, mask);

        DEBUG_LOG("Setting Gateway Address\n");
        w5500_set_GWADDR(device, gwaddr);

        BYTE rbuf[] = { 0,0,0,0,0,0 };
     
        socket_t* socket = NULL;

        DEBUG_LOG("Creating Socket\n");
        socket_create(device, SOCKET_PROTOCOL_TCP, 8044, &socket);

        waitMilliseconds(1000);
        //dump_socketStatus(socket);

        //socket->dest_ip.a0 = 192;
        //socket->dest_ip.a1 = 168;
        //socket->dest_ip.a2 = 100;
        //socket->dest_ip.a3 = 12;
        //socket->dest_port = 8888;

        socket->dest_ip.a0 = 172;
        socket->dest_ip.a1 = 217;
        socket->dest_ip.a2 = 3;
        socket->dest_ip.a3 = 196;
        socket->dest_port = 80;

        DEBUG_LOG("Connecting\n");
        socket_connect(socket);

        waitSeconds(2);
        dump_socketStatus(socket);

        WORD tempSize = 0x0400;

        BYTE *sendBuffer = (BYTE*) malloc(tempSize);
        BYTE *receiveBuffer = (BYTE*) malloc(tempSize);

        sprintf(sendBuffer,"GET /?a=1&b=2 HTTP/1.1\nHost: www.google.com\nUser-Agent: AppleIIgsSPI\nAccept: text/html\nAccept-Language: en-us,en;\n\n\n");

        DEBUG_LOG("sending...\n");
        socket_send(socket, sendBuffer, strlen(sendBuffer));

        waitSeconds(2);

        WORD receiveBytesAvailable = 0;

        clock_t cStart;
        clock_t cEnd;
        time_t tStart;
        time_t tEnd;

        cStart = clock();
        time(&tStart);

        int totalBytesReceived = 0;
        do {
                socket_receive_available(socket,&receiveBytesAvailable);
                if (receiveBytesAvailable>tempSize) receiveBytesAvailable = tempSize-1;
                memset(receiveBuffer,0,tempSize);
                socket_receive(socket, receiveBuffer, receiveBytesAvailable);
                totalBytesReceived += receiveBytesAvailable;
        } while (receiveBytesAvailable);

 
        cEnd = clock();
        time(&tEnd);

        double elapsedSeconds2 = ((double) cEnd - cStart) / 60.0F;
        double bytesPerSecond = ( (double)totalBytesReceived / elapsedSeconds2);
        DEBUG_LOG("Total Elapsed = %f seconds. BPS=%f\n",elapsedSeconds2,bytesPerSecond);
        DEBUG_LOG("Total Bytes Received = %d\n",totalBytesReceived);

        return;

        int longCount = 10000;
        while(longCount--) {

                memset(sendBuffer,0,tempSize);

                for(int bufpos = 0; bufpos < (tempSize-1); bufpos++) {
                        sendBuffer[bufpos] = (BYTE) (65+((longCount+bufpos)%26));
                }
                DEBUG_LOG("countdown=%d\n",longCount);

                clock_t cStart;
                clock_t cEnd;
                time_t tStart;
                time_t tEnd;

                cStart = clock();
                time(&tStart);
                socket_send(socket, sendBuffer, strlen(sendBuffer));
                cEnd = clock();
                time(&tEnd);

                double elapsedSeconds2 = ((double) cEnd - cStart) / 60.0F;
                double elapsedSeconds = difftime(tEnd, tStart);
                double bytesPerSecond = ( (double)tempSize / elapsedSeconds2);
                DEBUG_LOG("Total Elapsed = %f seconds. BPS=%f\n",elapsedSeconds2,bytesPerSecond);

                WORD receiveBytesAvailable = 0;
                memset(receiveBuffer,0,tempSize);

                socket_receive_available(socket,&receiveBytesAvailable);

                DEBUG_LOG("There are %d bytes available to read\n", receiveBytesAvailable);
                if (receiveBytesAvailable>tempSize) receiveBytesAvailable = tempSize-1;
                socket_receive(socket, receiveBuffer, receiveBytesAvailable);
                DEBUG_LOG("Received Data : %s\n", receiveBuffer);

                waitSeconds(3);



        }
        waitSeconds(60);

}



