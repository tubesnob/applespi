#include "../defaults.h"

#ifndef __UTILS_DOT_H__
#define __UTILS_DOT_H__

#define FORCE_CAST(var, type) *(type*)&var
#define LOW(x)  (x & 0xFF)
#define HIGH(x) ((x >> 8) & 0xFF)

void waitMilliseconds(int ms);
void waitSeconds(int seconds);

void DEBUG_LOG(const char *format, ...);

WORD gen_crc16(const BYTE *data, WORD size);

char *strdup(const char *s);

#endif

