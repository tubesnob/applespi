#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

#pragma ignore 0x0018
#pragma optimize -1

#ifndef __DEFAULTS_DOT_H__
#define __DEFAULTS_DOT_H__

typedef unsigned char  BYTE;
typedef unsigned short WORD;
typedef unsigned int   DWORD;

#endif



